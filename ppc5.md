---
title: 'Programming Checkpoint 5'
author: '黃明瀧 107021129'
documentclass: scrreprt
fontsize: 12pt
resource-path: ./assets/
toc: true
header-includes:
  - \usepackage{graphicx}
---

# `delay(n)` and `now()`

## Implementation Brief

First I declared a global data variable named `clock` to indicate elapsed time since boot up:

```C
__data __at (0x2a) char clock;
```

. Then, I went a step further to define both `delay()` and `now()` - both as **macros** inside the header file. I chose to write them as macros since I had a problem in which stacks of some threads are crashing into high addresses (where I hardcoded some global variables). The `now()` macro is pretty simple. It's just the `clock` value:

```C
#define now() clock
```

. The `delay()` macro sets the delay of the thread, stored inside a C array, to the desired value, and then spin in a loop until that delay value drops to zero:

```C
#define delay(time) \
    EA = 0; \
    delays[current_thread] = time; \
    EA = 1; \
    while (delays[current_thread]);
```

The remaining delay values are decreased each **time quantum** (described in sections below) if they're not zero:

```C
delay_count++;
if (delay_count == 32) {
    delay_count = 0;
    clock++;
    if (delays[0] != 0)
        delays[0]--;

    if (delays[1] != 0)
        delays[1]--;

    if (delays[2] != 0)
        delays[2]--;

    if (delays[3] != 0)
        delays[3]--;
}
```

## On the Choice of TQ

From the project checkpoint manual it reads:

> An important consideration is that delay() is not an exact delay but is a delay for “at least n time units” and “less than (n + 0.5) time units” for it to be acceptable (otherwise, it rounds up to n+1 time units, which would not be correct).  Of course, the more accurate the better, but there is an inherent limit on how accurate it can be. 
> Based on the above requirement, state your choice of time unit and provide your justification for how you think you can implement a delay() that meets the requirement above.

Here I propose a possible way to choose the time quantum for `delay()`.

### Background Information

Assuming that we're using 13-bit timer to set up interrupts, then

- Every 8192 clock cycles, the timer generates an interrupt.
- Each clock cycle is approimately $1\mu s$ (I got this value by examining the time indicated by the simulator)

Hence, we have that interrupts (that we use to invoke the scheduler) are generated every $`0.008192s`$.

### Derivation of the Time Quantum

#### In Theory

Let the interval between interrupts be $\epsilon (= 0.008192s)$.

Let the time quantum be denoted $T = n\epsilon$, where $n$ is a positive integer.

Imagine that we're in the worst possible case, where four threads $t_0, \dots, t_3$ that are `delay()`ed by $aT, \dots, dT$ respectively are to **wake at the same time**.

Choose the one that gets scheduled last, say $t_3$, then it must wait for additional time of $3\epsilon$ before running. On the other hand, the choice of $T$ must be made s.t. $t_3$ is run after waiting at most $1.5dT$. Thus,

$$dT + 3\epsilon = (3 + nd)\epsilon < \frac 3 2 nd\epsilon = \frac 3 2 dT$$
$$\Leftrightarrow 6 < nd, \; \forall d\in\mathbb{N}$$
$$\Leftrightarrow 6 < n$$

Hence, for all $T \geq 6\epsilon$, we can guarantee the `delay()` function to be precise enough.

#### In Reality

The longer the time quantum is, the more precise the `delay()` function is. Without violating the $\leq 1 \textrm{sec}$ rule, I chose $T = 32\epsilon$, which is about $0.25 \textrm{sec}$. It was previously $16\epsilon$, but I found that in extreme cases (i.e. `delay(1)`), it would fail to wake within $1.5T$. Changing that to $32\epsilon$ solves the problem.

# Parking Lot Example

## Basics

Since we have only 3 threads left (apart from the main thread) to utilize, a semaphore `sem_main` is used to prevent exceeding that constraint:

```C
// In main()
...
SemaphoreWait(sem_main);
ThreadCreate((FunctionPtr) Park1);

SemaphoreWait(sem_main);
...

// In Parkn()
...
    SemaphoreSignal(sem_main);
    ThreadExit();
}
...
```

. These global variables are identical to the corresponding ones used in the Python version:

```C
// The two parking slots
__data __at(0x3a) char slots[2];
// Semaphore to disallow concurrent access to slots
__data __at(0x3c) Semaphore sem_sync;
// Counting semaphore indicating #slots left
__data __at(0x3d) Semaphore sem_park;
```

. Since SDCC defaults to using register bank 0 and that the `__using` syntax can be only used with compile-time constant numbers, I created a huge macro to generate `Park1()` ~ `Park3()` functions that differ only in the register banks they use:

```C
/* Concat "Park" and number n to form a new
 * identifier "Parkn" */
#define FNAME(n) Park ## n

/* Macro to generate nth Park function */
#define PARK(n) \
void FNAME(n)(void) __using (n) { \
    SemaphoreWait(sem_park); \
    ...
```

, to prevent repetition in the C source file.

## The Output Format

I went straight to implement this to output to UART immediately (please refer to the `PrintToUART(status)` macro), with the following output format.

- There are two types of outputs, **enter** and **leave**.
- **enter** log format (example: `r1f3e00`):

    `r - <two-digit hex of random number> - <one-digit thread number> - e - <one-digit slot number> - <two-digit hex timestamp>`

- **leave** log format (example: `2l116`):

    `<one-digit thread number> - e - <one-digit slot number> - <two-digit hex timestamp>`

The resulting output from UART looks like this:

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.6\textwidth]{assets/output.png}
\end{center}
\end{figure}

```
r1f3e000 r162e100 2l116 r011e116 1l117 r012e117 2l118 r2b1e118 3l01f 1l143 ...
```

To help debugging, I've created a short Ruby script `parse.rb` to parse this output format and print them in a more human-readable manner. To use the script, do

```BASH
# ./parse.rb '<output string>'

$ ./parse.rb 'r1f3e000 r162e100 2l116 r011e116 1l117 r012e117 2l118 r2b1e118 3l01f
1l143 '
Thread 3 entered slot 0 at time   0   [3, _]  Rand: 31
Thread 2 entered slot 1 at time   0   [3, 2]  Rand: 22
Thread 2 left    slot 1 at time  22   [3, _]  Actu: 22  (expected 22)
Thread 1 entered slot 1 at time  22   [3, 1]  Rand: 1
Thread 1 left    slot 1 at time  23   [3, _]  Actu: 1   (expected 1)
Thread 2 entered slot 1 at time  23   [3, 2]  Rand: 1
Thread 2 left    slot 1 at time  24   [3, _]  Actu: 1   (expected 1)
Thread 1 entered slot 1 at time  24   [3, 1]  Rand: 43
Thread 3 left    slot 0 at time  31   [_, 1]  Actu: 31  (expected 31)
Thread 1 left    slot 1 at time  67   [_, _]  Actu: 43  (expected 43)
```
