/*
 * file: cooperative.h
 *
 * this is the include file for the cooperative multithreading
 * package.  It is to be compiled by SDCC and targets the EdSim51 as
 * the target architecture.
 *
 * CS 3423 Fall 2018
 */

#ifndef __COOPERATIVE_H__
#define __COOPERATIVE_H__

#define MAXTHREADS 4  /* not including the scheduler */
/* the scheduler does not take up a thread of its own */

// Initialize semaphore s to n
#define SemaphoreCreate(s, n) \
{ \
    s = n; \
}

// Concat underscore with parameter s
#define CNAME(s) _ ## s

// Concat parameter l with dollar sign
#define LABELNAME(l) l ## $

#define SemaphoreSignal(s) \
{ \
    __asm \
        inc CNAME(s) /* s++; */ \
    __endasm; \
}

// Wait for semaphore s
#define SemaphoreWaitBody(s, label) \
{ \
    __asm \
    LABELNAME(label): /* while (s <= 0); */ \
        mov A, CNAME(s) \
        jb ACC.7, LABELNAME(label) \
        jz LABELNAME(label) \
        dec CNAME(s) /* s--; */ \
    __endasm; \
}

#define SemaphoreWait(s) \
{ \
    /* SemaphoreWaitBody with unique label name */ \
    SemaphoreWaitBody(s, __COUNTER__) \
}

__data __at (0x2a) char clock;
__data __at (0x2b) char delay_count;
__data __at (0x2c) char delays[4];

typedef char ThreadID;
typedef void (*FunctionPtr)(void);
typedef char Semaphore;

ThreadID ThreadCreate(FunctionPtr);
void ThreadYield(void);
void ThreadExit(void);

// The current time in 0~255
#define now() clock

#define delay(time) \
    EA = 0; \
    delays[current_thread] = time; \
    EA = 1; \
    while (delays[current_thread]);

#endif // __COOPERATIVE_H__
