#include <8051.h>

#include "preemptive.h"

// Bitmap of active threads
__data __at (0x34) char active_thread_map;
// Current thread ID
__data __at (0x35) char current_thread;
// Temporaries
__data __at (0x36) char tmp_a;
__data __at (0x37) char tmp_b;
__data __at (0x36) int tmp;
// Saved SP positions for threads
__data __at (0x30) char saved_sp[4];

//===== For the scheduler
// Scheduler period records
__data __at (0x3e) char sched_count;
__data __at (0x3f) char sched;
//=====

__data __at (0x38) char sem_threads;
/* __data __at(0x70) char debug; */

// Macros to save / restore context.
// R0, R1, R6, and R7 are saved because they're used in functions
//   such as ThreadYield() and myTimer0Handler().
#define SAVESTATE \
{ \
    __asm \
        push ACC \
        push B \
        push DPL \
        push DPH \
        push PSW \
        \
        mov B, R0 \
        mov DPH, R1 \
        mov DPL, R6 \
        mov _tmp_b, R7 \
    __endasm; \
    saved_sp[current_thread] = SP; \
}

// Exact reverse operation of SAVESTATE.
#define RESTORESTATE \
{ \
    SP = saved_sp[current_thread]; \
    __asm \
        mov R7, _tmp_b \
        mov R6, DPL \
        mov R1, DPH \
        mov R0, B \
        pop PSW \
        pop DPH \
        pop DPL \
        pop B \
        pop ACC \
    __endasm; \
}

extern void main(void);

void Bootstrap(void) {
    // Timer 0 mode 0
    TMOD = 0;
    // Enable timer 0 interrupt; keep consumer polling
    // EA  -  ET2  ES  ET1  EX1  ET0  EX0
    IE = 0x82;  
    // Start timer 0
    TR0 = 1;

    // Initialize bitmap to "no threads active"
    active_thread_map = 0x0;
    // Initialize thread id to 0
    current_thread = 0x0;
    sched = 0x0;
    sched_count = 0x0;

    SemaphoreCreate(sem_threads, 4);

    delays[0] = 0;
    delays[1] = 0;
    delays[2] = 0;
    delays[3] = 0;
    delay_count = 0;
    clock = 0;

    // Create main (0) thread
    ThreadCreate(main);

    // Restore the fresh context for main thread
    RESTORESTATE;

    // Implicitly `RET` to main()
}

ThreadID ThreadCreate(FunctionPtr fp) {
    SemaphoreWait(sem_threads);
    EA = 0;
    // All available threads are in use, abort.
    if (active_thread_map == 0xf)
        return -1;

    // New thread's TID
    ThreadID id = 0xff;

    char i = 0;
    for (i = 0; i <= 3; i++) {
        // If i-th bit of active_thread_map is zero,
        //   then use i as id
        if (!((1 << i) & active_thread_map)) {
            id = i;
            break;
        }
    }

    // OR in the bit to active threads bitmap
    active_thread_map |= (1 << id);
    
    // Calculate starting point of stack.
    //   stack position = 0x(id+3)f (so that first pushed item
    //   will be at 0x40, 0x50, 0x60, and 0x70 resp.)
    char start_stack_pos = ((char) id + 3) * 16 + 0xf;

    // Save original SP position
    char orig_sp = SP;

    // Set SP temporarily to that new position
    SP = start_stack_pos;

    // Calculate PSW value
    char psw_val = id << 3;

    tmp = (int) ThreadExit;
    __asm
        push _tmp_a
        push _tmp_b
    __endasm;

    // Put fp into tmp (2-byte) and then push them as 1+1 byte
    tmp = (int) fp;
    __asm
        push _tmp_a
        push _tmp_b
    __endasm;

    // Push four fresh register values (0)
    __asm
        mov a, #0x0
        push acc
        push acc
        push acc
        push acc
    __endasm;

    // Push PSW value (for register bank selection)
    tmp_a = psw_val;
    __asm
        mov a, _tmp_a
        push acc
    __endasm;

    // Save new thread's SP, and restore old SP
    saved_sp[id] = SP;
    SP = orig_sp;

    EA = 1;
    return id;
}

void ThreadYield(void) {
    EA = 0;
    // Save old thread's state
    SAVESTATE;

    do {
        // Wrapped increment
        current_thread++;
        current_thread = current_thread % (char) 4;

        // If current_thread-th bit of active_thread_map is 1 (active),
        //   then choose this as next thread.
        tmp_a = (active_thread_map & (1 << current_thread));
        if (tmp_a) {
            break;
        }
    } while (1);

    // Restore new thread's state
    RESTORESTATE;
    EA = 1;
}

void ThreadExit(void) {
    EA = 0;
    active_thread_map &= ~(1 << current_thread);
    SemaphoreSignal(sem_threads);

    if (active_thread_map == 0)
        while (1) ;

    do {
        // Wrapped increment
        current_thread++;
        current_thread = current_thread % (char) 4;

        // If current_thread-th bit of active_thread_map is 1 (active),
        //   then choose this as next thread.
        tmp_a = (active_thread_map & (1 << current_thread));
        if (tmp_a) {
            break;
        }
    } while (1);

    RESTORESTATE;
    EA = 1;
}

void myTimer0Handler(void) {
    // Save old thread's state
    SAVESTATE;

    // sched is in range [0, ..., 2t - 2].
    // For example, in the situation where there are 3 threads active:
    // [0, 1, 2, 3, 4]
    //  -------  ----
    //  forth    back
    //
    // In this manner, threads are run in such sequence:
    // [0, 1, 2, 0, 2, 1, 2, 0, 1, 0, 2, ....]
    // This makes sure that no matter which thread is the consumer,
    //   both producers have chance to get access to empty buffer.

    // We'll use tmp_a as the thread count,
    //   so init it to 0
    tmp_a = 0;

    // Check each bit to see if that thread active
    if (active_thread_map & 0x01)
        tmp_a++;
    if (active_thread_map & 0x02)
        tmp_a++;
    if (active_thread_map & 0x04)
        tmp_a++;
    if (active_thread_map & 0x08)
        tmp_a++;

    // Make sched iterate through its range (See comment above)
    sched++;
    if (sched > tmp_a * 2 - 2)
        sched = 0;

    // On every round of sched, increase sched_count
    if (sched == 0)
        sched_count++;
    
    // Periodically switch between the two policies:
    // a) Go forth n times and then go back (n-1) times
    // a) Go back n times and then go forth (n-1) times
    // Doing so ensures fairness to all the threads
    if (sched_count & 0x8) {
        if (sched < tmp_a) {
            // Case I: go forth
            while (1) {
                // pick next
                current_thread++;
                current_thread = current_thread % (char) 4;

                // If current_thread-th bit of active_thread_map is 1 (active),
                //   then choose this as next thread.
                tmp_a = (active_thread_map & (1 << current_thread));
                if (tmp_a) {
                    break;
                }
            }
        } else {
            // Case II: go back
            while (1) {
                // pick previous
                current_thread--;
                if (current_thread > 4)
                    current_thread = 4;

                // If current_thread-th bit of active_thread_map is 1 (active),
                //   then choose this as next thread.
                tmp_a = (active_thread_map & (1 << current_thread));
                if (tmp_a) {
                    break;
                }
            }
        }
    } else {
        if (sched < tmp_a) {
            // Case I: go back
            while (1) {
                // pick previous
                current_thread--;
                if (current_thread > 4)
                    current_thread = 4;

                // If current_thread-th bit of active_thread_map is 1 (active),
                //   then choose this as next thread.
                tmp_a = (active_thread_map & (1 << current_thread));
                if (tmp_a) {
                    break;
                }
            }
            // Case II: go forth
        } else {
            while (1) {
                // pick next
                current_thread++;
                current_thread = current_thread % (char) 4;

                // If current_thread-th bit of active_thread_map is 1 (active),
                //   then choose this as next thread.
                tmp_a = (active_thread_map & (1 << current_thread));
                if (tmp_a) {
                    break;
                }
            }
        }
    }

    delay_count++;
    if (delay_count == 32) {
        delay_count = 0;
        clock++;
        if (delays[0] != 0)
            delays[0]--;

        if (delays[1] != 0)
            delays[1]--;

        if (delays[2] != 0)
            delays[2]--;

        if (delays[3] != 0)
            delays[3]--;
    }

    // Restore new thread's state
    RESTORESTATE;

    // Return from interrupt
    __asm
        reti
    __endasm;
}

/* void delay(char time) { */
/*     EA = 0; */
/*     delays[current_thread] = time; */
/*     EA = 1; */
/*     while (delays[current_thread]); */
/* } */
